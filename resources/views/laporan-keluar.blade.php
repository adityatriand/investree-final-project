<!DOCTYPE html>

<html lang="en">

<head>
    <title>Laporan Produk Masuk</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>
    <h5>Laporan dibuat pada tanggal {{ $date }}</h5>
    <div class="card mt-4">
        <div class="card-header">
            <h4>List Product Keluar</h4>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>No</th>
                        <th>Products</th>
                        <th>Customer</th>
                        <th>QTY</th>
                        <th>Harga Satuan Jual</th>
                        <th>Tanggal Masuk</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $products as $product )
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $product->product_nama }}</td>
                        <td>{{ $product->customer_nama }}</td>
                        <td>{{ $product->qty }}</td>
                        <td>{{ $product->harga_jual}}</td>
                        <td>{{ $product->tanggal }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="card mt-4">
        <div class="card-header">
            <h4>Detail Laporan Produk Keluar</h4>
        </div>
        <div class="card-body">
            <p>Total jenis produk: {{ $jenis_produk }} jenis</p>
            <p>Total barang keluar: {{ $barang_keluar }} barang</p>
            <p>Total pemasukan biaya: Rp. {{ $pemasukan }}</p>
        </div>
    </div>
</body>

</html>