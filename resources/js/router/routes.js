import Homepage from '../src/pages/Homepage.vue';

//product
import List_product from '../src/pages/product/List_product.vue';
import Add_product from '../src/pages/product/Add_product.vue';
import Edit_product from '../src/pages/product/Edit_product.vue';

//category
import List_category from '../src/pages/category/List_category.vue';
import Add_category from '../src/pages/category/Add_category.vue';
import Edit_category from '../src/pages/category/Edit_category.vue';

//product_masuk
import List_product_masuk from '../src/pages/laporan/product_masuk/List_product_masuk.vue';
import Add_product_masuk from '../src/pages/laporan/product_masuk/Add_product_masuk.vue';
import Edit_product_masuk from '../src/pages/laporan/product_masuk/Edit_product_masuk.vue';

//supplier
import List_supplier from '../src/pages/supplier/List_supplier.vue';
import Add_supplier from '../src/pages/supplier/Add_supplier.vue';
import Edit_supplier from '../src/pages/supplier/Edit_supplier.vue';

//product_keluar
import List_product_keluar from '../src/pages/laporan/product_keluar/List_product_keluar.vue';
import Add_product_keluar from '../src/pages/laporan/product_keluar/Add_product_keluar.vue';
import Edit_product_keluar from '../src/pages/laporan/product_keluar/Edit_product_keluar.vue';

//customer
import List_customer from '../src/pages/customer/List_customer.vue';
import Add_customer from '../src/pages/customer/Add_customer.vue';
import Edit_customer from '../src/pages/customer/Edit_customer.vue';



const routes = [

    //product
    {
        name: 'Homepage',
        path: '/',
        component: Homepage
    },


    //product
    {
        name: 'List_product',
        path: '/product',
        component: List_product
    },

    {
        name: 'Add_product',
        path: '/product/add',
        component: Add_product
    },

    {
        name: 'Edit_product',
        path: '/product/:id/edit',
        component: Edit_product
    },


    //category
    {
        name: 'List_category',
        path: '/category',
        component: List_category
    },

    {
        name: 'Add_category',
        path: '/category/add',
        component: Add_category
    },

    {
        name: 'Edit_category',
        path: '/category/:id/edit',
        component: Edit_category
    },


    //Product_MASUK
    {
        name: 'List_product_masuk',
        path: '/product_masuk',
        component: List_product_masuk
    },

    {
        name: 'Add_product_masuk',
        path: '/product_masuk/add',
        component: Add_product_masuk
    },

    {
        name: 'Edit_product_masuk',
        path: '/product_masuk/:id/edit',
        component: Edit_product_masuk
    },

    //supplier
    {
        name: 'List_supplier',
        path: '/supplier',
        component: List_supplier
    },

    {
        name: 'Add_supplier',
        path: '/supplier/add',
        component: Add_supplier
    },

    {
        name: 'Edit_supplier',
        path: '/supplier/:id/edit',
        component: Edit_supplier
    },


    //Product_KELUAR
    {
        name: 'List_product_keluar',
        path: '/product_keluar',
        component: List_product_keluar
    },

    {
        name: 'Add_product_keluar',
        path: '/product_keluar/add',
        component: Add_product_keluar
    },

    {
        name: 'Edit_product_keluar',
        path: '/product_keluar/:id/edit',
        component: Edit_product_keluar
    },

    //customer
    {
        name: 'List_customer',
        path: '/customer',
        component: List_customer
    },

    {
        name: 'Add_customer',
        path: '/customer/add',
        component: Add_customer
    },

    {
        name: 'Edit_customer',
        path: '/customer/:id/edit',
        component: Edit_customer
    },




]

export default routes;
