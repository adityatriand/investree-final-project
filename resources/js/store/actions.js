import router from '../router/routes';
import store from '../store';

const actions = {

    //get data
    async getCategories({ commit }) {
        const res = await axios.get('/api/categorys')
        commit('SET_CATEGORIES', res.data.categories)
    },

    async getProducts({ commit }) {
        const res = await axios.get('/api/products')
        commit('SET_PRODUCTS', res.data.products)
    },

    async getCustomers({ commit }) {
        const res = await axios.get('/api/customers')
        commit('SET_CUSTOMERS', res.data.customers)
    },

    async getSuppliers({ commit }) {
        const res = await axios.get('/api/suppliers')
        commit('SET_SUPPLIERS', res.data.suppliers)
    },

    async getProduct_masuks({ commit }) {
        const res = await axios.get('/api/product_masuks')
        commit('SET_PRODUCT_MASUKS', res.data.product_masuks)
    },

    async getProduct_keluars({ commit }) {
        const res = await axios.get('/api/product_keluars')
        commit('SET_PRODUCT_KELUARS', res.data.product_keluars)
    },

    async getInformationProduct({commit}){
        const res = await axios.get('/api/getInfoProducts')
        commit('SET_INFORMATION_PRODUCTS', res.data.info_products)
    },

    //delete
    deleteCategory({ commit }, categoryId) {
        if (confirm("Are you sure to delete this kategori ?")) {
            axios.delete('/api/categorys/' + categoryId)
            .then(res => {
                commit('SET_CATEGORY_DELETED', categoryId)
            }).catch(error => {
                console.log(error)
            })
        }
    },

    deleteProduct({ commit }, productId) {
        if (confirm("Are you sure to delete this product ?")) {
            axios.delete('/api/products/' + productId)
            .then(res => {
                commit('SET_PRODUCT_DELETED', productId)
            }).catch(error => {
                console.log(error)
            })
        }
    },

    deleteCustomer({ commit }, customerId) {
        if (confirm("Are you sure to delete this customer ?")) {
            axios.delete('/api/customers/' + customerId)
            .then(res => {
                commit('SET_CUSTOMER_DELETED', customerId)
            }).catch(error => {
                console.log(error)
            })
        }
    },

    deleteSupplier({ commit }, supplierId) {
        if (confirm("Are you sure to delete this supplier ?")) {
            axios.delete('/api/suppliers/' + supplierId)
            .then(res => {
                commit('SET_SUPPLIER_DELETED', supplierId)
            }).catch(error => {
                console.log(error)
            })
        }
    },

    deleteProduct_masuk({ commit }, product_masukId) {
        if (confirm("Are you sure to delete this product_masuk ?")) {
            axios.delete('/api/product_masuks/' + product_masukId)
            .then(res => {
                commit('SET_PRODUCT_MASUK_DELETED', product_masukId)
            }).catch(error => {
                console.log(error)
            })
        }
    },

    deleteProduct_keluar({ commit }, product_keluarId) {
        if (confirm("Are you sure to delete this product_keluar ?")) {
            axios.delete('/api/product_keluars/' + product_keluarId)
            .then(res => {
                commit('SET_PRODUCT_KELUAR_DELETED', product_keluarId)
            }).catch(error => {
                console.log(error)
            })
        }
    },
}

export default actions

