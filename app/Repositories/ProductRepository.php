<?php 

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductRepository{

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getAll()
    {
        return DB::table('categories')
            ->join('products', 'products.category_id', '=', 'categories.id',)
            ->select('products.*', 'categories.category_nama')
            ->orderBy('products.product_nama')
            ->get();
    }

    public function getById($id)
    {
        return $this->product
            ->where('id', $id)
            ->get();
    }

    public function save($data)
    {
        $product = new $this->product;

        $product->product_nama = $data['product_nama'];
        $product->product_harga = $data['product_harga'];
        $product->product_qty = $data['product_qty'];
        $product->category_id = $data['category_id'];

        $product->save();

        return $product->fresh();
    }

    public function update($data, $id)
    {
        $product = $this->product->find($id);

        $product->product_nama = $data['product_nama'];
        $product->product_harga = $data['product_harga'];
        $product->product_qty = $data['product_qty'];
        $product->category_id = $data['category_id'];

        $product->update();

        return $product;   
    }

    public function delete($id)
    {
        $product = $this->product->find($id);
        $product->delete();

        return $product;
    }

    public function getInfoProducts(){
        $info = [
            'total_product' => DB::table('products')->count(),
            'total_product_masuk' => DB::table('product_masuks')->count(),
            'total_product_keluar' => DB::table('product_keluars')->count(),
            'total_kategori' => DB::table('categories')->count(),
            'total_customer' => DB::table('customers')->count(),
            'total_supplier' => DB::table('suppliers')->count(),
        ];
        return $info;
    }
}

?>