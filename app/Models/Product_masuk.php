<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_masuk extends Model
{
    use HasFactory;

    protected $table = 'product_masuks';
    protected $guarded = ['id'];
    protected $fillable = [
        'product_id',
        'supplier_id',
        'qty',
        'harga_modal',
        'tanggal'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}